{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell rec {
  name = "luctor-env";

  nativeBuildInputs = [
  ];

  buildInputs = [
    hugo
    go
    nodejs
  ];
  
  shellHook = ''
  alias hss='hugo server --noHTTPCache'
  '';
}
