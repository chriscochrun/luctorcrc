---
title: "About"
description: "Faithful Through the Ages"
featured_image: '/Victor_Hugo-Hunchback.jpg'
tags: ["Loving Jesus"]
showWordCount: false
showReadingTime: false
showDate: false
showEdit: false
showTaxonomies: true
sharingLinks: false
layout: simple
---
It was at the time when the "Westward Ho!" movement was strong, in the year 1877, that a few migrating Hollanders of the Reformed persuasion settled in Phillips County, KS, near the geographical center of the United States. In these few rugged pioneers, we have what we may call the beginning of a struggling church in the midst of a struggling people. Very appropriately, this group of people was named "Luctor", meaning "I struggle".

These pious and faithful pioneers, in their efforts to win a living from the soil, did not neglect the feeding of their souls, for they were staunch and true Calvinists. At first they held religious services in their various homes, which consisted of sod houses and huts. The church was officially organized on November 12, 1885 - a total of 8 families, 20 communicant members, and 28 members by baptism. Luctor came into existence as a church, having the distinction of being the farthest west in the entire Christian Reformed denomination. It was a small beginning, but the church prospered in spite of crop failures, the Depression, and a devastating fire.

The first church was an 18' x 38' structure, replaced in 1894 with a 52' x 34' building with a seating capacity of 300. A nine room parsonage was erected in 1906, replaced with the present home in 1965. The church was bursting at the seams in the early twenties and a meeting was called to decide what to do. A devastating fire destroyed the church and school buildings on July 5, 1926, so the members were forced into the decision to construct a new building. The present church building was dedicated in 1927 and has served us well to the present time.


# Our Vision and Mission
- vision
- mission

# Our Team
[Shawn Richardson](/staff)

# Our Statement of Faith
There’s no doubt that teens in the United States are in trouble. Violence, sex, drugs, peer pressure, resentment and anger all plague them. TFC’s goal of connecting these teens with Jesus is the only remedy for such a problem. Through connecting teens with other teens, Christian adults and the message of Christ, TFC serves God by connecting teens with the truth of Jesus Christ…teaching and equipping them to live out the Great Commission.

1. We believe the Bible to be the inspired, the only infallible, authoritative Word of God.
2. We believe there is one God, eternally existent in three persons: Father, Son and Holy Spirit.
3. We believe in the deity of our Lord Jesus Christ, His virgin birth, in His sinless life, in His miracles, in His vicarious and atoning death through His shed blood and His bodily resurrection, in His ascension to the right hand of the Father, and in His personal return in power and glory.
4. We believe that for the salvation of lost and sinful men, regeneration by the Holy Spirit is absolutely essential.
5. We believe in the present ministry of the Holy Spirit by whose indwelling the Christian is enabled to live a godly life.
6. We believe that all mankind will be bodily resurrected, the saved to eternal life, and the unsaved to judgment and everlasting punishment.
8. We believe in the spiritual unity of believers in Christ.

