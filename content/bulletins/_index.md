---
title: "Bulletins"
date: 2023-04-02T12:00:00-05:00
groupByYear: true
showAuthorsBadges: true
layout: list
menu: main
cascade:
    showDate: true
    showWordCount: true
    showReadingTime: true
    showEdit: false
    showTaxonomies: true
    showSummary: true
    showBreadcrumbs: true
    sharingLinks: ['facebook', 'instagram', 'email', 'reddit']
    showHero: true
---
If you'd like to read the bulletins for each Sunday, check them out here!
