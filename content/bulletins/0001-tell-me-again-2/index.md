---
title: "Tell Me Again, Part 2"
date: 2023-04-02T15:24:04-05:00
tags: ["Chris & Abbie Cochrun", "Leadership Retreat", "Northern Valley",  "180 Encounter", "45th Anniversary"]
featured_image: ""
description: "Matthew 21:1-11"
summary: "Matthew 21:1-11"
showAuthor: false
authors: ["LuctorCRC"]
showAuthorsBadges: false
---

LUCTOR CHRISTIAN REFORMED CHURCH
April 2, 2023 – 10:00 AM
Palm Sunday Reading: Mark 11:1-11
Praise Song: Red 145 “Hosanna, Loud Hosanna”

Call to Worship: Praise the Lord. Praise the Lord from the heavens, praise him in the heights above.
Let them praise he name of the Lord, for his name alone is exalted. Praise the Lord. -Psalm 148:1,13

Silent Prayer and Prayer Response: Green 209 “We Are the Reason”

Greeting: People of God, be gathered together!
Like a hen with her brood beneath her wings, God has brought us together!
People of God, await Christ's coming!
Blessed is the one who comes in the name of the Lord of heaven! -based on Luke 13:34; 19:38

* Greet One Another
* Hymn of Worship: Green 173 “All Glory, Laud and Honor”

SERVICE OF CONFESSION
Preparatory Service

SERVICE OF THE WORD
Scripture: Matthew 21:1-11
Sermon: “Tell Me Again, Part 2”

RESPONSE TO THE WORD
* Hymn of Response: Green 508 “I Will Sing the Wondrous Story”
* Confession of Faith: The Apostles’ Creed
Announcements, Concerns, Pastoral Prayer
Offering: Cary Christian Center
* Doxology and Prayer of Dedication
* Benediction and Response: Red 198 “Celtic Alleluia”

EVENING WORSHIP SERVICE - 7:00 PM
Song Service
Call to Worship and Prayer of Invocation
Responsive Reading: Christ's Triumphal Entry (Green #638)

Prayer of confession:
Loving God, you rode a donkey and came in peace, humbled yourself and gave yourself for us.
We confess our lack of humility. As you entered Jerusalem, the crowds shouted "Hosanna: 'Save us now!'" On Good Friday they shouted "Crucify!" We confess our praise is often empty. We sing "Hosanna," but cry "Crucify." As the crowd laid their palms in front of you, you took no glory for yourself. We confess that we want to be accepted and take the easy way. We do not stay true to your will.
Forgive us Lord and help us to follow in the way of obedience. Amen.

Hymn of worship: Green 227 “Thine Is the Glory”
Prayer of intercession and Hymn of praise: Red 184 “Good Christians All, Rejoice and Sing”
Prayer of illumination and Sermon passage reading: Luke 19:28-40
Sermon: “Untie It”
Prayer of application and Hymn of response: Red 599 “Come, Christians, Join to Sing”
Offering: Elim Christian Schools
Doxology and Prayer of dedication
Benediction and Departing hymn: Green 175 vs, 1,5 “Hallelujah, What a Savior”

Offerings Next Week: AM –Moses’ (MAF); PM – Friendship Ministries
Greeters: Dave & Velma Jansonius, Melvin & Bonnie Kats
Good Friday: ElWynn Jansonius, Jeff & Kelly Vanderplas
Next Week: Phil & Liz Jansonius, Willard Jansonius, Cleo Jansonius
Song Service: Kenny Pakkebier

ACTIVITIES THIS WEEK

WEDNESDAY: 1:30 PM – Golden Hour Circle – Lesson 20 – Marcia has refreshments
6:30 – Bible Class
7:00 – Youth Group
THURSDAY: 7:00 PM - Women's Bible Study
ANNOUNCEMENTS

OUR CHURCH FAMILY: Elinor Richardson invites all of you to celebrate her first birthday with an Open House at the parsonage TODAY from 5-7pm. No gifts necessary.

GOOD FRIDAY combined service with Prairie View is THIS Friday, April 7th, at 7:00pm at Luctor.

THIS MORNING our offering will be for the Cary Christian Center which is heavily involved in the clean-up and care of the victims of the tornado in Rolling Fork MS.

SAVE THE DATE: Our Sunday School picnic will take place on Sunday, May 21st featuring special music from the Sunday School students during our morning worship and a special dinner to follow.

MEDITATION FOR HOLY WEEK: Palm Sunday is the kick-off for an entire week of liturgical celebrations leading up to the celebration of Christ's resurrection on Resurrection Sunday. As a way for us to keep in mind the significance of Holy Week, read what each of the special days means.
1. Maundy Thursday marks three key events in Jesus' last week: his washing of his disciples' feet, his institution of the Lord's Supper, and his new commandment to love one another. The name "Maundy Thursday" comes from the Latin mandatum novum, referring to the "new commandment" Jesus taught his disciples in John 13:34.
2. Good Friday - it's called "good" because of what Jesus' death means for the redemption of the world. Worship on this day may focus on three aims: to narrate and remember the events of Jesus' death, to open up the meaning of these events for understanding, and to invite worshipers to renewed prayer and dedication.
3. Holy Saturday, or sometimes called "Silent Saturday." Just as Jesus' friends and family spent the Saturday following his crucifixion praying and waiting, we too can use this day as a day of meditation, prayer, and preparation.
4. Easter - "All the hopes and expectations of Christians are realized in the resurrection of Jesus Christ from the dead, making Easter the most celebrative day of the church year."
- excerpts taken from The Worship Sourcebook, John D. Witvliet, editor.
