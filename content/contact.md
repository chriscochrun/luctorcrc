---
title: Contact
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
layout: "simple"
sharingLinks: false

---

# Luctor Christian Reformed Church
946 W. 1100 Road
Prairie View, KS 67647
 
785-973-2793
 
Contact:
Shawn Richardson
205-703-3044

or

Beth Ponstein
Secretary
785-854-7605
