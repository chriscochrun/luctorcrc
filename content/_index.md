---
title: "LuctorCRC"
date: 2023-04-01T13:02:05-05:00
description: "Faithful Through the Ages"
showHeadingAnchors: true
---

# Faithful Through the Ages

Luctor Christian Reformed Church is located in rural Prairie View, Kansas, in the midst of rolling pastureland and cultivated farmland. We have around 90 professing members, with 50% being under the age of 50 and 50% being over. We serve a broad area with many members driving a long distance to come to church.

Our church building is over 90 years old, with a beautiful sanctuary and basement fellowship area. The parsonage is next door.

Our church is broken down in Care Groups that take turns helping out with funerals or special occasions. 

We offer our youth Sunday School, Wednesday night Bible Class and Youth Group, and Vacation Bible School in June.

There is a women's Bible study, Golden Hour Circle.

Many people have commented on the great congregational singing we have in our church. We have a vibrant adult choir that sings often during the year.

## Worship Services
- 10:00 AM
- 2:00 PM (winter) or 7:00 PM (DST)

## Our Vision

## Our Mission
