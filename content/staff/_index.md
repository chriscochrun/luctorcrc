---
title: "Staff"
date: 2017-03-02T12:00:00-05:00
featured_image: "img/shawn-preaching.jpg"
groupByYear: false
layout: simple
cascade:
    showWordCount: false
    showReadingTime: false
    showEdit: false
    showTaxonomies: true
    showSummary: true
    showBreadcrumbs: true
    sharingLinks: ['facebook', 'instagram', 'email', 'reddit']
    showHero: false
---
# About the Pastor
Rev. Shawn Richardson has been serving as pastor at Luctor CRC since May of 2019. Shawn grew up in a Christian family in Chillicothe, Ohio and, upon graduating high school, went to Calvin College where he studied history. After graduation, Shawn moved to Austin, Texas where he worked for a non-profit and volunteered extensively at Sunrise Community Christian Reformed Church. He was blessed to have a pastor at Sunrise who helped show him how God was calling him to ministry. Shawn decided to answer the call from the Lord to enter into pastoral ministry in 2015. He earned a Master of Divinity at Beeson Divinity School in Birmingham, Alabama, as well as having completed the Ecclesiastical Program for Ministerial
Candidacy at Calvin Theological Seminary. He was ordained as a Minister of the Word in the Christian Reformed Church in August of 2020. Shawn’s hobbies include anything to do with cars, drinking tea, and playing with his kids. Shawn has been married to Erin since 2015. They have three children.

![Shawn and Erin](/img/shawn.jpg "Shawn and Erin Richardson")

## Our Elders
- ElWynn Jansonius
- Kenny Pakkebier
- Harlan Ponstein
- Jeff Vanderplas
- Alan Woodside

## Our Deacons
- Chris Cochrun
- Avery Thalheim
- Jake Tien
- Steve Tien  
- Cory VanDerVeen
