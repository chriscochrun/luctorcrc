{
  stdenv,
  lib,
  hugo,
  go
}:

stdenv.mkDerivation rec {
  name = "tfcconnection";
  pname = "tfcconnection";
  version = "0.0.1";

  src = ./.;

  buildInputs = [
    hugo
    go
  ];

  buildPhase = ''
  hugo
  '';

  installPhase = ''
  '';

  meta = with lib; {
    name = "tfcconnection";
    description = "TFC Connection Website";
    homepage = "";
    license = licenses.gpl3;
    maintainers = [ "chriscochrun" ];
    platforms = platforms.all;
  };
}
